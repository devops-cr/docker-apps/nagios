# Nagios

This is a two-project application where we are building Nagios Server and Nagios Clients. 
Nagios is a monitor application that can be installed on a server and it can monitor various clients' services. 
The services that are being monitored are based on a file that it is called `template` and it is located in `nagios_server/opt/nagios/etc/servers/template`. 

We are installing the latest (as of now) versions on Nagios Server image which are :

- **Nagios Core v4.4.6**
- **Nagios Plugins v 2.3.3**
- **Nagios NRPE v 4.0.3**

On Nagios Client image we are installing the latest packages (as of now) which are :

- **Nagios Plugins v 2.3.3**
- **Nagios NRPE (Nagios Remote Plugin Executor) v4.0.3**

To be able to run the application we have to create first our docker images. To do so:

    cd nagios_server
    docker build -t nagios_server .

We will now create a nagios client image

    cd nagios_client
    docker build -t nagios_client .

We can change these names to suit our needs but if we do so we have to change the image names from the `docker-compose.yml` file as well. 

When we create our images we are ready to run our application with the command below. 

    docker-compose up -d

With the current set up we have to point out that we will run two containers where the Nagios Server will monitor itself and the client we are creating. If we want to create and maintain more clients it is highly recommended to use the Ansible role to create the clients we need. 

Nagios Server is using host's port 9090 so in order to access the page we will open on a browser `http://DOCKER_HOST_IP_ADDRESS:9090/nagios` and when we will be prompted for a password we type 

username: `nagiosadmin`
password: `nagios`

Because the default theme is not easy to work with and quite outdated we are providing an environment variable which is called 
`CHANGE_THEME` and by default it is set to false. We can switch it to `true` if we want to change the nagios theme. 


